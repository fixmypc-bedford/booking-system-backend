const Sequelize = require('sequelize');
const SequelizeTokenify = require('sequelize-tokenify');

/*
 * Database Models
 * At least most should be shared between instances (website and booking system)
 */

module.exports = function(conn) {

    let exports = {};

    // Model Structures

    exports.User = conn.define('user', {
        username: Sequelize.TEXT,
        password: Sequelize.TEXT('long'),
    });

    /* (?USER?) Authorization Tokens -- Needs duping for staff users and customers */
    exports.Token = conn.define('token', {
        token: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
        },
        userId: Sequelize.INTEGER,
    });

    /* Permission Flags */
    exports.Flag = conn.define('flag', {
        userId: Sequelize.INTEGER,
        flagName: Sequelize.STRING,
        flagValue: Sequelize.BOOLEAN,
    });

    /* File Attachments */
    exports.Attachment = conn.define('attachment', {
        name: Sequelize.STRING,
        localPath: Sequelize.STRING,
        hash: Sequelize.STRING,
        type: Sequelize.STRING,
        userId: Sequelize.INTEGER,
    });

    exports.Customer = conn.define('customer', {

        /* Login Details */
        emailAddress: Sequelize.STRING,
        password: Sequelize.STRING,

        /* Customer Details */
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,

        addressId: Sequelize.INTEGER,
        
        homePhone: Sequelize.BIGINT(11),
        mobilePhone: Sequelize.BIGINT(11),

        /* GDPR Compliancy - Marketing Preferences */
        allowSms: Sequelize.BOOLEAN,
        allowMail: Sequelize.BOOLEAN,
        allowEmail: Sequelize.BOOLEAN,

        signature: Sequelize.INTEGER,

        /* Internal Notes on a Customer */
        notes: Sequelize.TEXT,

        /* Was the account registered in-store */
        local: Sequelize.BOOLEAN,
    });

    /* An Address */
    exports.Address = conn.define('address', {
        houseNumber: Sequelize.STRING,
        streetName: Sequelize.STRING,
        townName: Sequelize.STRING,
        countyName: Sequelize.STRING,
        postCode: Sequelize.STRING,

        notes: Sequelize.TEXT,
    });

    exports.Order = conn.define('order', {
        customerId: Sequelize.INTEGER,
        userId: Sequelize.INTEGER,

        deviceName: Sequelize.STRING,
        hasAttachments: Sequelize.BOOLEAN,

        userName: Sequelize.STRING,
        userPassword: Sequelize.STRING,

        priorityService: Sequelize.BOOLEAN,

        cost: Sequelize.BIGINT,
    });

    exports.OrderAttachment = conn.define('orderAttachment', {
        orderId: Sequelize.INTEGER,
        attachmentId: Sequelize.INTEGER,
        notes: Sequelize.TEXT,
        userId: Sequelize.INTEGER,
    });

    exports.OrderNote = conn.define('orderNote', {
        orderId: Sequelize.INTEGER,
        userId: Sequelize.INTEGER,
        note: Sequelize.TEXT,
    });

    exports.OrderChecklistItem = conn.define('orderChecklistItem', {
        itemId: Sequelize.INTEGER,
        itemValue: Sequelize.TEXT,
    });

    exports.ChecklistGroup = conn.define('checklistGroup', {
        name: Sequelize.STRING,
        description: Sequelize.STRING,
    });

    exports.ChecklistItem = conn.define('checklistItem', {
        description: Sequelize.TEXT,
        type: Sequelize.TEXT,
        groupId: Sequelize.INTEGER,
    });

    exports.OauthState = conn.define('oauthState', {
        client_id: Sequelize.TEXT,
        code: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
        },
        redirect_uri: Sequelize.TEXT,
        scope: Sequelize.TEXT,
        state: Sequelize.TEXT,
        expires: Sequelize.INTEGER,
    });

    exports.OauthToken = conn.define('oauthToken', {
        client_id: Sequelize.TEXT,
        access_token: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
        },
        token_type: Sequelize.TEXT,
        expires_in: Sequelize.INTEGER,
        refresh_token: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
        },
        userId: Sequelize.INTEGER,
    });

    exports.Application = conn.define('application', {
        client_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
        },
        name: Sequelize.TEXT,
        description: Sequelize.TEXT,
        powerLevel: Sequelize.INTEGER,
    });

    // Unique Tokens
    SequelizeTokenify.tokenify(exports.Token);

    // Model Associations

    exports.User.hasMany( exports.Token );
    exports.User.hasMany( exports.OauthToken );
    exports.User.hasMany( exports.Flag );
    exports.User.hasMany( exports.Attachment );

    exports.Token.belongsTo( exports.User );
    exports.OauthToken.belongsTo( exports.User );

    exports.Flag.belongsTo( exports.User );

    exports.Attachment.belongsTo( exports.User );
    exports.Attachment.belongsTo( exports.OrderAttachment );

    exports.Customer.hasOne( exports.Address );
    exports.Customer.hasOne( exports.Attachment );
    exports.Customer.hasMany( exports.Order );

    exports.Order.belongsTo( exports.Customer );
    exports.Order.belongsTo( exports.User );
    exports.Order.hasMany( exports.OrderAttachment );
    exports.Order.hasMany( exports.OrderChecklistItem );
    exports.Order.hasMany( exports.OrderNote );

    exports.OrderAttachment.belongsTo( exports.Order );
    exports.OrderAttachment.hasOne( exports.Attachment );
    exports.OrderAttachment.belongsTo( exports.User );

    exports.OrderNote.belongsTo( exports.Order );
    exports.OrderNote.belongsTo( exports.User );

    exports.OrderChecklistItem.belongsTo( exports.Order );

    exports.ChecklistGroup.hasMany( exports.ChecklistItem );

    exports.ChecklistItem.belongsTo( exports.ChecklistGroup);

    return exports;
}