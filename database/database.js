const config = require('../config.js');
const Sequelize = require('sequelize');

module.exports.conn = new Sequelize(config.database);
module.exports.models = require('./models.js')(module.exports.conn);

module.exports.conn.sync();