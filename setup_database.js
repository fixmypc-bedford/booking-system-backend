// Sets up the database with example data
const database = require('./database/database.js');
const hasher = require('password-hash-and-salt');

const details = {
    username: "admin",
    password: "admin",
};

hasher(details.password).hash((err, hash) => {
    database.models.User.create({
        username: details.username,
        password: hash,
    })
    .then( user => {
        console.log("Added Admin user.");
        process.exit();
    }) 
    .catch( err => {
        throw err;
    });
});

database.models.Application.create({
    name: "FixMyPC",
    description: "FixMyPC Internal System",
    powerLevel: 100
});