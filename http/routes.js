// Pulls all the routes in
console.log("Routes registered.");
module.exports = (httpd) => {
    // Internal Routes for APIs and such
    require('./routes/oauth.js')(httpd);
    require('./routes/user.js')(httpd);
    require('./routes/static.js')(httpd);

    // Main Routes
    require('./routes/main/main.js')(httpd);
};