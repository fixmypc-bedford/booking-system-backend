'use strict';

const config = require('../config.js');
const manifest = require('../package.json');
const database = require('../database/database.js');

const authProvider = require('./authProvider.js'); // Replace with the bearer token

// Hapi JS API
let Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const Lout = require('lout');
const Handlebars = require('handlebars');

const path = require('path');

const server = new Hapi.Server({
    port: 8080,
    load: {
        sampleInterval: 1000
    }
});



// Register our auth provider and lookup method
let serverPlugins = [];

serverPlugins.push( server.register(Vision) );
serverPlugins.push( server.register(Inert) );
//server.register(Lout);
serverPlugins.push( server.register(authProvider) );

Promise.all(serverPlugins)
.then( results => {
    server.auth.strategy('user','token', {
        validate: (token) => {
            // Find a token and its accompanying user.
            console.log("Checking a token: ", token)
            return database.models.Token.findOne({
                where: { token },
                include: [
                    {
                        model: database.models.User,
                        include: [
                            { model: database.models.Token },
                            { model: database.models.Flag }
                        ]
                    },
                ]
            })
            .then( token => {
                if (!token) return null;
                return token.user;
            });
        }
    });
    
    server.events.on('log', (event, tags) => {
    
        if (tags.error) {
            console.log(`Server error: ${event.error ? event.error.message : 'unknown'}`);
        }
    });

    server.views({
        engines: {
            html: Handlebars,
        },
        relativeTo: path.join(__dirname, '..'),
        path: 'templates/content',
        partialsPath: 'templates/partials',
        layoutPath: 'templates/layouts',
        helpersPath: 'templates/helpers',
        layout: 'base',
        context: {
            title: 'Home',
            application: manifest
        },
    });
    
    server.path( path.join(__dirname, '../static') );

    // Add our routes
    require('./routes.js')(server);

    // Boot the HTTPD (to the moon)
    server.start( err => {
        if (err) {
            throw err;
            return;
        }
        console.log(`Hapi HTTPD Started`);
    });
});

module.exports = server;