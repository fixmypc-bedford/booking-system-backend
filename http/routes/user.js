// User routes (auth mainly)
const database = require('../../database/database.js');

const Joi = require('joi');
const Boom = require('boom');

const hasher = require('password-hash-and-salt');

module.exports = (httpd) => {
    httpd.route({
        method: 'GET',
        path: '/api/',
        config : {
            auth: 'user',
        },
        handler: function (request, reply) {
            reply(request.auth.credentials);
        }
    });

    // Gets a user (All except password!)
    httpd.route({
        method: 'GET',
        path: '/api/user/{username}',
        config: {
            auth: 'user'
        },
        handler: function( request, reply) {
            database.models.User.findOne({ where: { username: request.params.username }, attributes: { exclude: ['password'] } })
            .then( user => {
                if (!user) return reply(Boom.notFound('No such user!'));
                reply(user);
            });
        }
    });

    // Gets current user
    httpd.route({
        method: 'GET',
        path: '/api/user',
        config: {
            auth: 'user',
        },
        handler: function( request, reply) {
            reply( request.auth.credentials );
        }
    });

    // Updates the (current) user password
    httpd.route({
        method: 'POST',
        path: '/api/user/password',
        config: {
            auth: 'user',
            validate: {
                payload: {
                    password: Joi.string(),
                    username: Joi.string().optional(),
                    current: Joi.string().when('username',{
                        is: Joi.exist(),
                        then: Joi.optional(),
                        otherwise: Joi.required(),
                    }),
                }
            }
        },
        handler: function(request, reply) {
            if (request.payload.username) {
                // Update another users password.
                database.models.User.findOne({ where: { username: request.payload.username } })
                .then( user => {
                    if (!user) return reply(Boom.notFound('No such user'));
                    hasher(request.payload.password).hash( (err, hash) => {
                        if (err) return reply(Boom.internal(err));

                        database.models.User.update({
                            password: hash,
                        }, {
                            where: { id: user.id }
                        })
                        .then( arr => {
                            reply({ success: true });
                        });
                    });
                });
            } else {
                // Update our own password

                // Check the current password
                hasher(request.payload.current).verifyAgainst(request.auth.credentials.password, (err, res) => {
                    if (err) return reply(Boom.internal(err));
                    if (!res) return reply(Boom.forbidden('Incorrect Current Password'));

                    hasher(request.payload.password).hash( (err, hash) => {
                        if (err) return reply(Boom.internal(err));

                        database.models.User.update({
                            password: hash,
                        }, {
                            where: { id: request.auth.credentials.id }
                        })
                        .then( arr => {
                            reply({ success: true });
                        });
                    });
                });
            }
        }
    });

    // Adds a user
    httpd.route({
        method: 'PUT',
        path: '/api/user',
        config: {
            auth: 'user',
            validate: {
                payload: {
                    username: Joi.string(),
                    password: Joi.string(),
                }
            }
        },
        handler: function (request, reply) {
            database.models.User.findOne({ where: { username: request.payload.username } })
            .then( user => {
                if (user) return reply(Boom.notAcceptable('User already exists'));

                hasher(request.payload.password).hash( ( err, password ) => {
                    if (err) return reply(Boom.internal(err));
                    database.models.User.create({
                        username: request.payload.username,
                        password: password,
                    })
                    .then( user => {
                        reply({
                            success: true,
                            userId : user.id
                        });
                    });
                });

            });
        }
    });

    // Attempts login, returns token on success
    httpd.route({
        method: 'POST',
        path: '/api/user/login',
        config: {
            validate: {
                payload: {
                    username: Joi.string(),
                    password: Joi.string(),
                }
            }
        },
        handler: function(request, h) {
            // Attempt auth
            return new Promise( (resolve, reject) => {
                database.models.User.findOne({ where: { username: request.payload.username } })
                .then( user => {
                    if (!user) return reply(Boom.forbidden('Invalid Authentication Details'));
                    let hash = user.password;
                    hasher(request.payload.password).verifyAgainst(hash, (err, res) => {
                        if (err) {
                            return resolve(Boom.internal(err));
                        }
                        if (!res) {
                            return resolve(Boom.forbidden('Invalid Authentication Details'));
                        }
                        // Success
                        database.models.Token.create({ userId: user.id })
                        .then( token => {
                            resolve(token);
                        });
                    });
                });
            });
        }
    });
}