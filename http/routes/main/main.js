// Main Routes - TEMP
module.exports = (httpd) => {
    httpd.route({
        method: 'GET',
        path: '/',
        config: { auth: false, },
        handler: (req, h) => {
            return h.view('home', { title: 'DUUR PLANT'});
        }
    });
}