// oAuth2 Flow - Relatively follows RFC6749
// Needs to URGENTLY be switched to using Hapi-Auth-Bearer-Token for the sake of sanity.
const database = require('../../database/database.js');

const Joi = require('joi');
const Boom = require('boom');

module.exports = (httpd) => {
    

    // Auth endpoint

    /*
        This may need to be altered to allow a single page application to wrap around it.

        ~
        response_type: code (Must be `code` or `token`, REQUIRED)
        client_id: someid (REQUIRED),
        redirect_uri: someuri (OPTIONAL, depends on server, must match one listed by the application),
        scope: a,b,c (OPTIONAL, list of different datas the application wishes to access)
        state: abc (OPTIONAL, applications own unique data that is passed back to the application at flow end)

        token: abc (REQUIRED, this is a nonspec. When this endpoint is called from the auth page it exchanges an internal token for an access_token)

        ~

        WHEN USING RESPONSE TYPE CODE:
        RFC6749 4.1.1

        When authenticated, the server redirects the user to the supplied redirect_uri along with the `code` generated
        and the `state` parameter the application supplied these are added to the redirect_uri as QUERY PARAMS

        WHEN USING RESPONSE TYPE TOKEN:
        RFC6749 4.2.1

        Rather than responding with a generated code, responds with access_token, token_type, expires_in along with scope (the same provided) and state (same provided)
        This is classed being less secure as its being conducted 100% in plain view of the user, normally restricted to lower access applications

        ~

        Upon failiure instead `error`, `error_description` , `error_uri` (OPTIONAL) and `state` must be sent instead
        as per RFC6749 4.1.2.1 for `token` response_type see RFC6749 4.2.2.1
    */
    httpd.route({
        method: 'GET',
        path: '/api/oauth/authorize',
        config: {
            validate: {
                query: {
                    response_type: Joi.string().required(),
                    redirect_uri: Joi.string().required(),
                    client_id: Joi.string().required(),
                    scope: Joi.string().required(),
                    state: Joi.string().optional(),
                    token: Joi.string().required(),
                }
            }
        },
        handler: function(req, h) {
            if (req.query.response_type.toLowerCase() === "code") {
                return new Promise((resolve, reject) => {
                    database.models.Application.findOne({ where: { client_id: req.query.client_id } })
                    .then(app => {
                        if (app === null) return reject(Boom.forbidden("Invalid `client_id`, Application not found"));

                        database.models.Token.findOne({ where: { token: req.query.token }, include: [{ model: database.models.User }] })
                        .then( token => {
                            if (token === null) return reject(Boom.forbidden("Invalid `token`"));

                            // In future we'll expect a valid scope and redirect_uri
                            database.models.OauthState.create({
                                /*
                                client_id: Sequelize.TEXT,
                                code: {
                                    type: Sequelize.UUID,
                                    defaultValue: Sequelize.UUIDV4,
                                },
                                redirect_uri: Sequelize.TEXT,
                                scope: Sequelize.TEXT,
                                state: Sequelize.TEXT,
                                expires: Sequelize.INTEGER,
                                */

                                client_id: app.client_id,
                                redirect_uri: req.query.redirect_uri,
                                scope: req.query.scope,
                                state: req.query.state,
                                expires: 123,
                            }).then( stateToken => {
                                return resolve(token);
                            });
                        });
                    });
                });
            } else if (req.query.response_type.toLowerCase() === "token") {

            } else {
                return Boom.notAcceptable("Invalid `response_type`, refer to RF6749 for allowed types.");
            }
        }
    });

    // Access token endpoint (POST)
    /*
        This endpoint is called by the application with the code supplied during the start of the flow
        in return the server exchanges the code with an access_token

        ~

        = Auth code grant type:
        RFC6749 4.1.3

        grant_type: `authorization_code` (REQUIRED)
        code: blahblah (REQUIRED, Code supplied at start)
        redirect_uri: blah (REQUIRED. exact URI supplied at the start)
        client_id: blahblah (REQUIRED, Clients ID)

        ~
        
        = Password grant type:
        RFC6749 4.3

        grant_type: `password` (REQUIRED)
        username: CaptainObvious
        password: capta1n0bvious123
        scope: a,b,c (OPTIONAL, list of different datas the application wishes to access)

        ~

        If authorization was successful the server must respond in JSON with
        `access_token`, `token_type`, `expires_in`, `refresh_token` and any additional data
        by practice most apis respond with an `info` object containing extra data

    */

    httpd.route({
        method: 'POST',
        path: '/api/oauth/token',
        config :{
            validate: {
                payload: {
                    grant_type: Joi.string(),
                    client_id: Joi.string(),
                    client_secret: Joi.string(),
                    refresh_token: Joi.string(),
                    code: Joi.string(),
                    redirect_uri: Joi.string(),
                }
            }
        },
        handler: (req, h) => {
            // Returns access token etc (same endpoint as refreshing)
        }
    });
}