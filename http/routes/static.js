// Static Routes - Serves static assets.

module.exports = (httpd) => {
    httpd.route({
        method: 'GET',
        path: '/static/{file*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true,
            }
        }
    });
}