const Boom = require('Boom');

// oAuth Token Authorization
// For endpoints requiring auth the token is expected to be set as an Authorization header
// or as a "token" cookie.

let internals = {};

internals.callback = function(server, options) {
    return {
        authenticate: function(req, h) {
            if (!req.headers.authorization) return h.unauthenticated(Boom.unauthorized());
            
            const parts = req.headers.authorization.split(/\s+/);

            if (parts.length < 2) return h.unauthenticated(Boom.unauthorized());
            if (parts[0].toLowerCase() != "token") return h.unauthenticated(Boom.unauthorized());

            return options.validate(parts[1])
            .then( creds => {
                if (!creds) return h.unauthenticated(Boom.unauthorized());
                return h.authenticated({
                    credentials: creds,
                });
            });
        }
    };
};

exports.plugin = {
    name: "token-scheme",
    version: "1.0.0",
    register: (server, options) => {
        server.auth.scheme('token', internals.callback);
    }
};