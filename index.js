// FixMyPC - Booking System
// Backend API
// Written by Thomas Edwards for FixMyPC
// Copyright (c) 2017

const env = require('env');

process.on('unhandledRejection', error => {
    // Will print "unhandledRejection err is not defined"
    console.log('unhandledRejection', error);
  });

// Configuration
const config = require('./config.js');

// Database
const database = require('./database/database.js');

// HTTPD
const http = require('./http/http.js');