module.exports = {
    /* Passed directly to Sequelize */
    database: {
        host: "localhost",
        port: 3306,
        username: "fixmypc",
        password: "",
        database: "fixmypc",
        dialect: "mysql",
    },
    /* Passed directly to HapiJS */
    httpd: {
        host: '0.0.0.0',
        port: 8080,
    }
};